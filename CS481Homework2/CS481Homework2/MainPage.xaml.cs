﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481Homework2
{
    

    public partial class MainPage : ContentPage
    {
        int currentState = 1; //currentState tracks if the first or second(not implemented) number is being edited.
        double fNumber, number;

        public MainPage()
        {
            InitializeComponent();
            OnClearButton(this, null);
        }

        void OnSelectNumber(object sender, EventArgs e) //Function is called when a number is pressed on the calculator
        {
            Button button = (Button)sender; 
            string pressed = button.Text; //Whatever button is pressed, the button's text (0-9) is set to a string

            if (result.Text == "0" || currentState < 0) //If the label display's 0, when the user presses "0", it will not change the result label
            {
                result.Text = "";
                if (currentState < 0)
                    currentState *= -1;
            }

            result.Text += pressed; //add the pressed string to the result label.

            if (double.TryParse(result.Text, out number))
            {
                result.Text = number.ToString("N0");
                if (currentState == 1) 
                {
                    fNumber = number;
                }

            }
        }

        void OnClearButton(object sender, EventArgs e) //Function is called when the Clear button is pressed.
        {
            fNumber = 0;
            currentState = 1;
            result.Text = "0";

        }
    }
}
